package com.springbootmybatis.service;

import com.springbootmybatis.entity.Board;
import com.springbootmybatis.repository.BoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardService {

    private final BoardRepository boardRepository;

    public List<Board> getList(){
        return boardRepository.getList();
    };

    public Board get(int id){
        return boardRepository.get(id);
    };

    public int save(Board parameter){
        // 조회하여 리턴된 정보
        Board board = boardRepository.get(parameter.getId());
        if(board == null){
            boardRepository.save(parameter);
        }else{
            boardRepository.update(parameter);
        }
        boardRepository.save(parameter);
        return parameter.getId();
    };
    public boolean delete(int id) {
        boardRepository.delete(id);
        return true;
    };

}
