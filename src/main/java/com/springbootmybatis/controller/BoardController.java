package com.springbootmybatis.controller;

import com.springbootmybatis.entity.Board;
import com.springbootmybatis.service.BoardService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/board")
public class BoardController {

    private final BoardService boardService;

    @GetMapping
    public List<Board> getList(){
        return boardService.getList();
    };

    @GetMapping("{id}")
    public Board get(@PathVariable int id){
        return boardService.get(id);
    };

    @GetMapping("/save")
    public int save(Board board){
        boardService.save(board);
        return board.getId();
    };

    @GetMapping("/delete/{id}")
    public boolean delete(@PathVariable int id){
        Board board = boardService.get(id);
        if(board == null){
            return false;
        }
        boardService.delete(id);
        return true;
    };

}
