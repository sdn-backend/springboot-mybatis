package com.springbootmybatis.repository;

import com.springbootmybatis.entity.Board;
import org.springframework.stereotype.Repository;

import java.util.List;

// 나중에 xml 파일이랑 연동되면서 쿼리가 실행 된다
@Repository
public interface BoardRepository {

    List<Board> getList();

    Board get(int id);

    void save(Board board);

    void update(Board board);

    void delete(int id);

}
