# Spring Boot + MyBatis 개발
> Spring Boot, MyBatis, MySQL, Lombok, Gradle

## 목차
1. [구현 방법](#1-구현-방법)
2. [프로젝트 구조](#2-프로젝트-구조)

## 1 구현 방법
### 1. 초기 세팅
- Spring Web
- Lombok
- MyBatis Framework
- MySQL Driver
- Spring Boot Devtools
- Spring Configuration Processor

### 2. 라이브러리 설정
- `DatabaseConfig.class`
```java
@Configuration
public class DatabaseConfig {

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }
}
```

### 3.MyBatis + API
1. Entity
2. Repository
    - 해당 클래스에서 `Mybatis` 랑 맵핑이 된다. 
3. Service
4. Controller
5. Mapper SQL xml 개발
`Board.xml`
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.springbootmybatis.repository.BoardRepository">
    <select id="getList" parameterType="com.springbootmybatis.entity.Board" resultType="com.springbootmybatis.entity.Board">
        SELECT
            ID,
            TITLE,
            CONTENTS
        FROM T_BOARD
        ORDER BY REG_DATE DESC;
    </select>

    <select id="get" parameterType="com.springbootmybatis.entity.Board" resultType="com.springbootmybatis.entity.Board">
        SELECT
            ID,
            TITLE,
            CONTENTS,
            REG_DATE
        FROM T_BOARD
        WHERE ID = #{ID};
    </select>

    <insert id="save" parameterType="com.springbootmybatis.entity.Board" useGeneratedKeys="true" keyProperty="id">
        INSERT INTO T_BOARD
        (
            TITLE,
            CONTENTS,
            REG_DATE
        )
        VALUES
        (
            #{title},
            #{contents},
            NOW()
        );
    </insert>

    <update id="update" parameterType="com.springbootmybatis.entity.Board" >
        UPDATE T_BOARD
        SET
            TITLE = #{title},
            CONTENTS = #{contents}
        WHERE ID = #{id};
    </update>

    <delete id="delete" parameterType="int" >
        DELETE FROM T_BOARD
        WHERE ID = #{id};
    </delete>
</mapper>
```
6. spring mybatis 설정
`MybatisConfig.class`
```java
@Configuration
@MapperScan(basePackages = "com.springbootmybatis.repository")
public class MybatisConfig {

    @Bean
    public SqlSessionFactory sqlSessionFactory(@Autowired DataSource dataSource, ApplicationContext applicationContext) throws Exception{
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setMapperLocations(applicationContext.getResources("classpath:mybatis/sql/*.xml"));
        SqlSessionFactory factory = factoryBean.getObject();
        factory.getConfiguration().setMapUnderscoreToCamelCase(true);
        return factoryBean.getObject();
    }

    @Bean
    public SqlSessionTemplate sqlSessionTemplate(@Autowired SqlSessionFactory sqlSessionFactory){
        return new SqlSessionTemplate(sqlSessionFactory);
    }
}
```

## 2 프로젝트 구조
```
📦 project
├─ main/java
│  ├─ config ⭐
│  │  ├─ DB 설정
│  │  └─ Mybatis 설정
│  ├─ entity
│  │  └─ Board
│  ├─ repository
│  │  └─ BoardRepository
│  ├─ service
│  │  └─ BoardService
│  └─ controller
│     └─ BoardController
└─ main/resource
   ├─ mybatis ⭐
   │  └─ sql
   │     └─ Board.xml
   ├─ static
   ├─ templates
   └─ application.properties
```